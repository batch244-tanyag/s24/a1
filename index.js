console.log("hello world");

// 1. In the S24 folder, create an activity folder and an index.html and index.js file inside of it.
// 2. Link the index.js file to the index.html file.
// 3. Create a variable getCube and use the exponent operator to compute for the cube of a number. (A cube is any number raised to 3)
	

	const getCube = Math.pow(2,3);


// 4. Using Template Literals, print out the value of the getCube variable with a message of The cube of <num> is…
	

	console.log(`the cube of 2 is ${getCube}`);


// 5. Create a variable address with a value of an array containing details of an address.

	const address = ["248", "Washinton Ave NW", "california 90011"]	
	
// 6. Destructure the array and print out a message with the full address using Template Literals.

	const  [houseNumber, city, state] = address;
	console.log(`I live at ${houseNumber} ${city} ${state}`);

// 7. Create a variable animal with a value of an object data type with different animal details as it’s properties.

	const animal = {
		type :"saltwater crocodile",
		weight : "1075 kgs",
		length : "20 ft 3 in"
	};

// 8. Destructure the object and print out a message with the details of the animal using Template Literals.

	const {type, weight, length} = animal
	console.log(`Lolong was a ${type}. He weighed at ${weight} with a measurement of  ${length}. `);


// 9. Create an array of numbers.

	const numbers = [1, 2, 3, 4, 5];

		

// 10. Loop through the array using forEach, an arrow function and using the implicit return statement to print out the numbers.
	numbers.forEach((numbers)=>{
			console.log(parseInt(`${numbers}`))
		});

// 11. Create a variable reduceNumber and using the reduce array method and an arrow function console log the sum of all the numbers in the array.


	let reduceNumber = numbers.reduce((previousValue, currentValue) =>
	    previousValue + currentValue);
	console.log(reduceNumber);


// 12. Create a class of a Dog and a constructor that will accept a name, age and breed as it’s properties.
	class Dog {
			constructor(name, age, breed) {
				this.name =name;
				this.age = age;
				this.breed = breed;

			}
		}



// 13. Create/instantiate a new object from the class Dog and console log the object.
	let myDog = new Dog();
		myDog.name = "Frankie";
		myDog.age ="5"
		myDog.breed = "Miniature Dachshund";
		console.log(myDog);

// 14. Create a git repository named S24.
// 15. Initialize a local git repository, add the remote link and push to git with the commit message of Add activity code.
// 16. Add the link in Boodle.




